/* jshint devel:true */

!(function() {

  // ONLY FOR DEVELOPMENT
  // DELETE IFRAMES FOR FAST LOADING PAGE
  $(document).ready(function() {
    $('iframe').each(function() {
      // $(this).remove();
    });
  });

  /**
   * Header
   */
  var header = {

    init: (function() {
      $(window).scroll(function() {
        if ($(window).scrollTop() > 100) {
          $('header').removeClass('transparent');
        } else {
          $('header').addClass('transparent');
        }
      });
    }())

  };

  /**
   * Set common height for row
   * Show or hide
   */
  var table = {

    setHeight: function($itemScreen) {
      var
        $table = $itemScreen.find('.items-table'),
        tables = $table.length,
        rows = $table.find('.row').length,
        heights = [],
        height;

      for (var r = 0; r < rows / tables; r++) {

        if (tables === 2) {
          heights[0] = $table.eq(0).find('.row').eq(r).height();
          heights[1] = $table.eq(1).find('.row').eq(r).height();

          if ((heights[0] > heights[1])) {
            height = heights[0];
          } else if ((heights[1] > heights[0])) {
            height = heights[1];
          } else if ((heights[0] === heights[1])) {
            height = heights[0];
          }

          height = height + 20;

          if ((heights[0] == heights[1])) {} else {
            $table.eq(0).find('.row').eq(r).css('height', height);
            $table.eq(1).find('.row').eq(r).css('height', height);
          }
        }

        if (tables === 3) {
          heights[0] = $table.eq(0).find('.row').eq(r).height();
          heights[1] = $table.eq(1).find('.row').eq(r).height();
          heights[2] = $table.eq(2).find('.row').eq(r).height();

          if ((heights[0] > heights[1]) && heights[0] > heights[2]) {
            height = heights[0];
          } else if ((heights[1] > heights[0]) && heights[1] > heights[2]) {
            height = heights[1];
          } else if ((heights[2] > heights[0]) && heights[2] > heights[1]) {
            height = heights[2];
          } else if ((heights[0] === heights[1]) && heights[0] > heights[2]) {
            height = heights[0];
          }

          height = height + 20;

          if ((heights[0] == heights[1]) && (heights[0] == heights[2]) &&
            (heights[1] == heights[0]) && (heights[1] == heights[2]) &&
            (heights[2] == heights[0]) && (heights[2] == heights[1])) {} else {
            $table.eq(0).find('.row').eq(r).css('height', height);
            $table.eq(1).find('.row').eq(r).css('height', height);
            $table.eq(2).find('.row').eq(r).css('height', height);
          }
        }

      }

    },

    setColor: function() {
      $('.items-table .color').each(function() {
        var color = $(this).html();

        $(this).html('');
        $(this).css('background', color);
      });
    },

    init: (function() {
      $(window).load(function() {

        // Set height
        $('.item-screen').each(function() {
          table.setHeight($(this));
        });

        // Show/hide table
        $('.items-show-table-button').on('click', function() {
          $(this).closest('.item-screen').find('.items-table').toggleClass('show');
          $(this).closest('.item-screen').find('.item-review').toggleClass('show');

          $(this).closest('.item-screen').find('.item-second-name').toggleClass('show');

          $(this).toggleClass('reveal');

          table.setHeight($(this).closest('.item-screen'));

          $(window).trigger('resize').trigger('scroll');
        });

        table.setColor();

      });
    }())

  };


}());
