# Imagn.ru

## About
Landing page for the Russian startup.
[http://andreykeske.com/projects/imagn.ru/index.html](http://andreykeske.com/projects/imagn.ru/index.html)


## Development
In app folder index.html is compiled from app/layout/index.html by gulp 'fileinclude' task

### Build
```
$ gulp build
```

### Server and development

```
$ gulp serve
```


---

![](http://andreykeske.com/assets/images/git/yo.png)
![](http://andreykeske.com/assets/images/git/bower.png)
![](http://andreykeske.com/assets/images/git/gulp.png)
![](http://andreykeske.com/assets/images/git/sass.png)